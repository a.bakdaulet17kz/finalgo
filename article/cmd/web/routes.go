package main

import (
	"github.com/go-chi/chi"
	"net/http"
)

func (a *app) routes() http.Handler {
	mux := chi.NewRouter()

	mux.Get("/articles", a.Get)
	mux.Get("/articles/{id:[0-9]+}", a.GetOne)
	mux.Post("/articles/create", a.Create)
	mux.Post("/articles/edit/{id:[0-9]+}", a.Update)
	mux.Post("/articles/delete/{id:[0-9]+}", a.Delete)

	return mux
}
