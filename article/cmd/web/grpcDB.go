package main

import (
	"context"
	"gitlab.com/a.bakdaulet17kz/FinalGo/article/pkg/models"
	"gitlab.com/a.bakdaulet17kz/FinalGo/pb"
	"io"
	"log"
)

func (a *app) getArticles() []models.Article {
	ctx := context.Background()
	req := &pb.GetArticlesRequest{}

	conn := a.connectDBService()
	c := pb.NewArticlesDBServiceClient(conn)
	defer conn.Close()

	stream, err := c.GetArticles(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GetArticles RPC %v", err)
	}
	defer stream.CloseSend()

	var articles []models.Article

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			log.Fatalf("error while reciving from GetArticles RPC %v", err)
		}

		tempArticle := models.Article{
			ID:       int(res.GetArticle().GetId()),
			AuthorID: int(res.GetArticle().GetAuthorId()),
			Title:    res.GetArticle().GetTitle(),
			Content:  res.GetArticle().GetContent(),
		}
		articles = append(articles, tempArticle)
	}
	return articles
}

func (a *app) GetArticle(id int) models.Article {
	ctx := context.Background()
	conn := a.connectDBService()
	c := pb.NewArticlesDBServiceClient(conn)
	defer conn.Close()

	request := &pb.GetArticleRequest{Id: int32(id)}

	response, err := c.GetArticle(ctx, request)
	if err != nil {
		log.Fatalf("error while calling GetArticle RPC %v", err)
	}

	tempArticle := models.Article{
		ID:       int(response.GetArticle().GetId()),
		Title:    response.GetArticle().GetTitle(),
		Content:  response.GetArticle().GetContent(),
		AuthorID: int(response.GetArticle().GetAuthorId()),
	}

	return tempArticle
}

func (a *app) CreateArticle(article models.Article) int {
	ctx := context.Background()
	conn := a.connectDBService()
	c := pb.NewArticlesDBServiceClient(conn)
	defer conn.Close()

	tempArticle := &pb.Article{
		Id:       int32(article.ID),
		Title:    article.Title,
		Content:  article.Content,
		AuthorId: int32(article.AuthorID),
	}

	request := &pb.CreateArticleRequest{Article: tempArticle}

	response, err := c.CreateArticle(ctx, request)
	if err != nil {
		log.Fatalf("error while calling CreateArticle RPC %v", err)
	}

	return int(response.GetId())
}

func (a *app) DeleteArticle(id int) string {
	ctx := context.Background()
	conn := a.connectDBService()
	c := pb.NewArticlesDBServiceClient(conn)
	defer conn.Close()

	request := &pb.DeleteArticleRequest{Id: int32(id)}

	response, err := c.DeleteArticle(ctx, request)
	if err != nil {
		log.Fatalf("error while calling DeleteArticle RPC %v", err)
	}

	return response.GetResult()
}

func (a *app) UpdateArticle(article models.Article) string {
	ctx := context.Background()
	conn := a.connectDBService()
	c := pb.NewArticlesDBServiceClient(conn)
	defer conn.Close()

	tempArticle := &pb.Article{
		Id:       int32(article.ID),
		Title:    article.Title,
		Content:  article.Content,
		AuthorId: int32(article.AuthorID),
	}

	request := &pb.UpdateArticleRequest{Article: tempArticle}

	response, err := c.UpdateArticle(ctx, request)
	if err != nil {
		log.Fatalf("error while calling UpdateArticle RPC %v", err)
	}

	return response.GetResult()
}
