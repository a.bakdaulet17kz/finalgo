package main

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/a.bakdaulet17kz/FinalGo/article/pkg/models"
	"net/http"
	"strconv"
)

func (a *app) Get(w http.ResponseWriter, r *http.Request) {
	articles := a.getArticles()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(articles)
}

func (a *app) GetOne(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	article := a.GetArticle(id)
	json.NewEncoder(w).Encode(article)
}

func (a *app) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var article models.Article
	_ = json.NewDecoder(r.Body).Decode(&article)
	article.ID = a.CreateArticle(article)
	json.NewEncoder(w).Encode(article)
}

func (a *app) Update(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	var article models.Article
	_ = json.NewDecoder(r.Body).Decode(&article)
	article.ID = id
	a.UpdateArticle(article)
	json.NewEncoder(w).Encode(article)
}

func (a *app) Delete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	a.DeleteArticle(id)
}
