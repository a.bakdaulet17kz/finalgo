package main

import (
	"flag"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
)

type app struct {
	errorLog *log.Logger
	infoLog  *log.Logger
}

func main() {

	addr := flag.String("addr", ":8080", "HTTP network address")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	app := &app{
		errorLog: errorLog,
		infoLog:  infoLog,
	}

	mux := app.routes()

	srv := &http.Server{
		Addr:     *addr,
		Handler:  mux,
		ErrorLog: app.errorLog,
	}

	app.infoLog.Printf("Starting a server on %s", *addr)
	err := srv.ListenAndServe()
	app.errorLog.Fatal(err)
}

func (a *app) connectDBService() *grpc.ClientConn {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	return conn
}
