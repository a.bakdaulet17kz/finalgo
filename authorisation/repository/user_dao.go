package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/a.bakdaulet17kz/FinalGo/authorisation/domain"
	"gitlab.com/a.bakdaulet17kz/FinalGo/authorisation/foundation/rest_errors"
	"gitlab.com/a.bakdaulet17kz/FinalGo/authorisation/utils/crypto"
	"log"
)

const (
	queryGetAllUsers            = "SELECT u.id, u.email, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id"
	queryInsertUser             = "INSERT INTO users (email, password) VALUES ($1,$2) RETURNING id"
	queryInsertUserWithRole     = "INSERT INTO users (email, password, role_id) VALUES ($1,$2, $3) RETURNING id"
	queryGetUser                = "SELECT u.id, u.email, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id WHERE id=$1"
	queryUpdateUser             = "UPDATE users SET email=$1, password=$2, role_id=$3 WHERE id=$4"
	queryDeleteUser             = "DELETE FROM users WHERE id=$1"
	queryFindByEmailAndPassword = "SELECT u.id, u.email, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id WHERE email=$1 AND password=$2"
	queryGetRoleIdByRoleName    = "SELECT role_id FROM roles WHERE role_name=$1"
)

type UserRepository struct {
	Pool     *pgxpool.Pool
	ErrorLog *log.Logger
	InfoLog  *log.Logger
}

func (r *UserRepository) Get(id int64) (*users.User, rest_errors.RestErr) {
	row := r.Pool.QueryRow(context.Background(), queryGetUser, id)
	user := &users.User{}

	err := row.Scan(&user.Id, &user.Email, &user.Role)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, rest_errors.NewNotFoundError("User does not exist")
		} else {
			return nil, rest_errors.NewInternalServerError("internal server error", err)
		}
	}
	return user, nil
}

func (r *UserRepository) Save(user *users.User) (*users.User, rest_errors.RestErr) {
	var userId int64
	row := r.Pool.QueryRow(context.Background(), queryInsertUser, user.Email, user.Password)
	err := row.Scan(&userId)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("internal server error", err)
	}
	user.Id = userId
	return user, nil
}

func (r *UserRepository) Update(user *users.User) (*users.User, rest_errors.RestErr) {
	_, err := r.Pool.Exec(context.Background(), queryUpdateUser, user.Email, crypto.GetMd5(user.Password), user.Role, int(user.Id))
	if err != nil {
		return nil, rest_errors.NewInternalServerError("internal server error", err)
	}
	return user, nil
}

func (r *UserRepository) Delete(id int64) rest_errors.RestErr {
	_, err := r.Pool.Exec(context.Background(), queryDeleteUser, id)
	if err != nil {
		return rest_errors.NewInternalServerError("internal server error", err)
	}
	return nil

}

func (r *UserRepository) FindByEmailAndPassword(email string, password string) (*users.User, rest_errors.RestErr) {
	row := r.Pool.QueryRow(context.Background(), queryFindByEmailAndPassword, email, password)
	user := &users.User{}
	err := row.Scan(&user.Id, &user.Email, &user.Role)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, rest_errors.NewNotFoundError("user or password is incorrect")
		} else {
			return nil, rest_errors.NewInternalServerError("internal server error", err)
		}
	}
	return user, nil
}
