package users

import "encoding/json"

type PublicUser struct {
	Id    int64  `json:"id"`
	Email string `json:"username"`
	Role  string `json:"role"`
}

type PrivateUser struct {
	Id       int64  `json:"id"`
	Email    string `json:"username"`
	Role     string `json:"role"`
	Password string `json:"password"`
}

func (users Users) Marshall(isPublic bool) []interface{} {
	result := make([]interface{}, len(users))
	for index, user := range users {
		result[index] = user.Marshall(isPublic)
	}
	return result
}

func (user *User) Marshall(isPublic bool) interface{} {
	if isPublic {
		return PublicUser{
			Id:    user.Id,
			Email: user.Email,
			Role:  user.Role,
		}
	}

	userJson, _ := json.Marshal(user)
	var privateUser PrivateUser
	json.Unmarshal(userJson, &privateUser)
	return privateUser
}
