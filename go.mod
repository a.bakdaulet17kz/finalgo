module gitlab.com/a.bakdaulet17kz/FinalGo

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jackc/pgx/v4 v4.10.1
	github.com/go-chi/chi v1.5.4
	github.com/jackc/pgx/v4 v4.10.1
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
