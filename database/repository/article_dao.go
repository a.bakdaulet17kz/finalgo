package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/a.bakdaulet17kz/FinalGo/pb"
	"os"
)

type ArticleRepository struct {
	Pool *pgxpool.Pool
}

const (
	queryGetAllArticles = "SELECT * FROM article"
	queryGetArticle     = "SELECT * FROM article WHERE id=$1"
	queryInsertArticle  = "INSERT INTO article (author_id, title, content) VALUES ($1,$2,$3) RETURNING id"
	queryUpdateArticle  = "UPDATE article SET title=$1, content=$2 WHERE id=$3"
	queryDeleteArticle  = "DELETE FROM article WHERE id=$1"
)

func (r *ArticleRepository) GetAll() []*pb.Article {
	rows, err := r.Pool.Query(context.Background(), queryGetAllArticles)
	if err != nil {
		fmt.Fprint(os.Stderr, "QueryRow failed: %v\n", err)
	}
	defer rows.Close()

	var articles []*pb.Article
	for rows.Next() {
		article := &pb.Article{}
		err = rows.Scan(&article.Id, &article.AuthorId, &article.Title, &article.Content)
		if err != nil {
			fmt.Fprint(os.Stderr, "Error: %v\n", err)
		}
		articles = append(articles, article)
	}
	if err = rows.Err(); err != nil {
		fmt.Fprint(os.Stderr, "Error: %v\n", err)
	}
	return articles
}

func (r *ArticleRepository) Get(id int64) *pb.Article {
	row := r.Pool.QueryRow(context.Background(), queryGetArticle, id)
	article := &pb.Article{}
	err := row.Scan(&article.Id, &article.AuthorId, &article.Title, &article.Content)
	if err != nil {
		fmt.Fprint(os.Stderr, "Error: %v\n", err)
	}
	return article
}

func (r *ArticleRepository) Save(article *pb.Article) int32 {
	var articleId int32
	row := r.Pool.QueryRow(context.Background(), queryInsertArticle, article.AuthorId, article.Title, article.Content)
	err := row.Scan(&articleId)
	if err != nil {
		fmt.Fprint(os.Stderr, "Error: %v\n", err)
	}
	article.Id = articleId
	return articleId
}

func (r *ArticleRepository) Update(article *pb.Article) string {
	_, err := r.Pool.Exec(context.Background(), queryUpdateArticle, article.Title, article.Content, int(article.Id))
	if err != nil {
		fmt.Fprint(os.Stderr, "Error: %v\n", err)
	}
	return "successfully updated"
}

func (r *ArticleRepository) Delete(id int64) string {
	_, err := r.Pool.Exec(context.Background(), queryDeleteArticle, id)
	if err != nil {
		fmt.Fprint(os.Stderr, "Error: %v\n", err)
	}
	return "successfully deleted"
}
