package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/a.bakdaulet17kz/FinalGo/database/repository"
	"gitlab.com/a.bakdaulet17kz/FinalGo/pb"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

type app struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	dbPool   *repository.ArticleRepository
	pb.UnimplementedArticlesDBServiceServer
}

func main() {
	connString := "postgres://postgres:admin@localhost:5432/finalgo"
	dsn := flag.String("dsn", connString, "PostgreSQL data source name")

	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	conn, err := openDB(*dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer conn.Close()

	var checkDBConn string
	err = conn.QueryRow(context.Background(), "select 'Connected to database'").Scan(&checkDBConn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	infoLog.Print(checkDBConn)

	app := app{
		errorLog: errorLog,
		infoLog:  infoLog,
		dbPool:   &repository.ArticleRepository{Pool: conn},
	}

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	pb.RegisterArticlesDBServiceServer(s, &app)
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}

func openDB(dsn string) (*pgxpool.Pool, error) {
	conn, err := pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
