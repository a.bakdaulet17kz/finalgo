package main

import (
	"context"
	"gitlab.com/a.bakdaulet17kz/FinalGo/pb"
	"log"
)

func (a *app) GetArticles(req *pb.GetArticlesRequest, stream pb.ArticlesDBService_GetArticlesServer) error {
	log.Printf("GetArticles function was invoked with %v \n", req)
	articles := a.dbPool.GetAll()
	for i := 0; i < len(articles); i++ {
		res := &pb.GetArticlesResponse{Article: articles[i]}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error while sending GetArticlesResponse: %v", err.Error())
		}
	}
	return nil
}

func (a *app) GetArticle(ctx context.Context, req *pb.GetArticleRequest) (*pb.GetArticleResponse, error) {
	log.Printf("GetArticle function was invoked with %v \n", req)
	id := req.GetId()

	result := a.dbPool.Get(int64(id))

	res := &pb.GetArticleResponse{
		Article: result,
		Result:  "success",
	}

	return res, nil
}

func (a *app) CreateArticle(ctx context.Context, req *pb.CreateArticleRequest) (*pb.CreateArticleResponse, error) {
	log.Printf("CreateArticle function was invoked with %v \n", req)
	article := req.GetArticle()

	id := a.dbPool.Save(article)

	res := &pb.CreateArticleResponse{
		Id:     id,
		Result: "success",
	}

	return res, nil
}

func (a *app) DeleteArticle(ctx context.Context, req *pb.DeleteArticleRequest) (*pb.DeleteArticleResponse, error) {
	log.Printf("DeleteArticle function was invoked with %v \n", req)
	id := req.GetId()

	result := a.dbPool.Delete(int64(id))

	res := &pb.DeleteArticleResponse{
		Result: result,
	}

	return res, nil
}

func (a *app) UpdateArticle(ctx context.Context, req *pb.UpdateArticleRequest) (*pb.UpdateArticleResponse, error) {
	log.Printf("UpdateArticle function was invoked with %v \n", req)
	article := req.GetArticle()

	result := a.dbPool.Update(article)

	res := &pb.UpdateArticleResponse{
		Result: result,
	}

	return res, nil
}
